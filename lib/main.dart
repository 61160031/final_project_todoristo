import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoristo/page/homepage.dart';

import './providers/task.dart';

void main() => runApp(ToDoListApp());

class ToDoListApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => TaskProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.pink,
          accentColor: Colors.yellow[700],
        ),
        title: 'To Do List',
        home: Homepage(),
      ),
    );
  }
}
